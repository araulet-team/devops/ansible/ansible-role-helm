# Ansible Role Helm [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Role for installing Helm on Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git@gitlab.com:araulet-team/devops/ansible/ansible-role-helm.git
  name: araulet.helm
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.helm
    become: true
    vars:
      helm_install: true
      helm_version: v2.11.0
    tags: [ 'helm' ]
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet